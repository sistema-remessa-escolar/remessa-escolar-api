package br.com.mvsistema.remessaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.remessaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.remessaapi.filter.EscolaFilter;
import br.com.mvsistema.remessaapi.model.Escola;
import br.com.mvsistema.remessaapi.service.EscolaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/cliente")
@Api(value = "API Rest EscolaResource")
public class EscolaResource {
	
	@Autowired
	private EscolaService escolaService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	/**
	 * Pesquisar Escola
	 */
	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Escola")
	public Page<Escola> search(EscolaFilter escolaFilter, Pageable pageable) {
		return escolaService.filter(escolaFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o Escola
	 */
	@GetMapping("/{code}")
	@ApiOperation(value = "Retorna um Escola pelo Codigo")
	public ResponseEntity<Escola> buscarPeloCodigo(@PathVariable Long code) {
		Escola escola = escolaService.buscarEscolaPeloCodigo(code);
		return escola != null ? ResponseEntity.ok(escola) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar Escola
	 */
	@PostMapping
	@ApiOperation(value = "criar um Escola")
	public ResponseEntity<Escola> criar(@Valid @RequestBody Escola escola, HttpServletResponse response) {
		Escola escolaSalvo = escolaService.save(escola);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, escolaSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(escolaSalvo);
	}
	
	/**
	 * Atualizar Escola
	 */
	@PutMapping("/{codigo}")
	@ApiOperation(value = "Atualizar um Escola")
	public ResponseEntity<Escola> atualizar(@PathVariable Long codigo, @Valid @RequestBody Escola escola) {
		Escola escolaSalva = escolaService.atualizar(codigo, escola);
		return ResponseEntity.ok(escolaSalva);
	}
	

	/**
	 * Deletar Escola
	 */
	@DeleteMapping("/{code}")
	@ApiOperation(value = "Deletar um Escola")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		escolaService.delete(code);
	}

}
