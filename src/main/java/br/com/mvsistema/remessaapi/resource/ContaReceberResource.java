package br.com.mvsistema.remessaapi.resource;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.remessaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.remessaapi.filter.ContaReceberFilter;
import br.com.mvsistema.remessaapi.model.ContaReceber;
import br.com.mvsistema.remessaapi.service.ContaReceberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/contaReceber")
@Api(value = "API Rest ContaReceberResource")
public class ContaReceberResource {
	
	@Autowired
	private ContaReceberService contaReceberService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	/**
	 * Pesquisar ContaReceber
	 */
	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de ContaReceber")
	public Page<ContaReceber> search(ContaReceberFilter contaReceberFilter, Pageable pageable) {
		return contaReceberService.filter(contaReceberFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o ContaReceber
	 */
	@GetMapping("/{code}")
	@ApiOperation(value = "Retorna um ContaReceber pelo Codigo")
	public ResponseEntity<ContaReceber> buscarPeloCodigo(@PathVariable Long code) {
		ContaReceber contaReceber = contaReceberService.buscarContaReceberPeloCodigo(code);
		return contaReceber != null ? ResponseEntity.ok(contaReceber) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar ContaReceber
	 */
	@PostMapping
	@ApiOperation(value = "criar um ContaReceber")
	public ResponseEntity<ContaReceber> criar(@Valid @RequestBody ContaReceber contaReceber, HttpServletResponse response) {
		ContaReceber contaReceberSalvo = contaReceberService.save(contaReceber);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, contaReceberSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(contaReceberSalvo);
	}
	
	/**
	 * Atualizar ContaReceber
	 */
	@PutMapping("/{codigo}")
	@ApiOperation(value = "Atualizar um ContaReceber")
	public ResponseEntity<ContaReceber> atualizar(@PathVariable Long codigo, @Valid @RequestBody ContaReceber contaReceber) {
		ContaReceber contaReceberSalva = contaReceberService.atualizar(codigo, contaReceber);
		return ResponseEntity.ok(contaReceberSalva);
	}
	

	/**
	 * Deletar ContaReceber
	 */
	@DeleteMapping("/{code}")
	@ApiOperation(value = "Deletar um ContaReceber")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		contaReceberService.delete(code);
	}
	
	/**
	 * Pesquisar ContaReceber
	 */
	@GetMapping("/total")
	@ApiOperation(value = "Retorna total contas receber")
	public BigDecimal totalRecebimento(ContaReceberFilter contaReceberFilter) {
		return contaReceberService.totalRecebimentos(contaReceberFilter);
	}

}
