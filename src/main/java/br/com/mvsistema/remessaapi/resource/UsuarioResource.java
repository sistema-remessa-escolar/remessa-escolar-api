package br.com.mvsistema.remessaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.remessaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.remessaapi.filter.UsuarioFilter;
import br.com.mvsistema.remessaapi.model.Usuario;
import br.com.mvsistema.remessaapi.service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/usuario")
@Api(value = "API Rest UsuarioResource")
public class UsuarioResource {
	
	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	/**
	 * Pesquisar Usuario
	 */
	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Usuario")
	public Page<Usuario> search(UsuarioFilter usuarioFilter, Pageable pageable) {
		return usuarioService.filter(usuarioFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o Usuario
	 */
	@GetMapping("/{code}")
	@ApiOperation(value = "Retorna um Usuario pelo Codigo")
	public ResponseEntity<Usuario> buscarPeloCodigo(@PathVariable Long code) {
		Usuario usuario = usuarioService.buscarUsuarioPeloCodigo(code);
		usuario.setSenha("");
		return usuario != null ? ResponseEntity.ok(usuario) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar Usuario
	 */
	@PostMapping
	@ApiOperation(value = "criar um Usuario")
	public ResponseEntity<Usuario> criar(@Valid @RequestBody Usuario usuario, HttpServletResponse response) {
		Usuario usuarioSalvo = usuarioService.save(usuario);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, usuarioSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioSalvo);
	}
	
	/**
	 * Atualizar Usuario
	 */
	@PutMapping("/{codigo}")
	@ApiOperation(value = "Atualizar um Usuario")
	public ResponseEntity<Usuario> atualizar(@PathVariable Long codigo, @Valid @RequestBody Usuario usuario) {
		Usuario usuarioSalva = usuarioService.atualizar(codigo, usuario);
		return ResponseEntity.ok(usuarioSalva);
	}
	

	/**
	 * Deletar Usuario
	 */
	@DeleteMapping("/{code}")
	@ApiOperation(value = "Deletar um Usuario")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		usuarioService.delete(code);
	}

}
