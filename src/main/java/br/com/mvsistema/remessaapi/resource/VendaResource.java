package br.com.mvsistema.remessaapi.resource;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.remessaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.remessaapi.filter.VendaFilter;
import br.com.mvsistema.remessaapi.model.Venda;
import br.com.mvsistema.remessaapi.service.VendaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.jasperreports.engine.JRException;

@RestController
@RequestMapping("/venda")
@Api(value = "API Rest VendaResource")
public class VendaResource {
	
	@Autowired
	private VendaService vendaService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	/**
	 * Pesquisar Venda
	 */
	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Venda")
	public Page<Venda> search(VendaFilter vendaFilter, Pageable pageable) {
		return vendaService.filter(vendaFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o Venda
	 */
	@GetMapping("/{code}")
	@ApiOperation(value = "Retorna um Venda pelo Codigo")
	public ResponseEntity<Venda> buscarPeloCodigo(@PathVariable Long code) {
		Venda venda = vendaService.buscarVendaPeloCodigo(code);
		return venda != null ? ResponseEntity.ok(venda) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar Venda
	 */
	@PostMapping
	@ApiOperation(value = "criar um Venda")
	public ResponseEntity<Venda> criar(@Valid @RequestBody Venda venda, HttpServletResponse response) {
		Venda vendaSalvo = vendaService.save(venda);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, vendaSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(vendaSalvo);
	}
	
	/**
	 * Atualizar Venda
	 */
	@PutMapping("/{codigo}")
	@ApiOperation(value = "Atualizar um Venda")
	public ResponseEntity<Venda> atualizar(@PathVariable Long codigo, @Valid @RequestBody Venda venda) {
		Venda vendaSalva = vendaService.atualizar(codigo, venda);
		return ResponseEntity.ok(vendaSalva);
	}
	

	/**
	 * Deletar Venda
	 */
	@DeleteMapping("/{code}")
	@ApiOperation(value = "Deletar um Venda")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		vendaService.delete(code);
	}
	
	/**
	 * Relatorio de Venda por Id.
	 * @param id
	 * @return
	 * @throws JRException
	 */
	@GetMapping("/relatorio/pedido-remessa")
	public ResponseEntity<byte[]> relatorioPedidoRemessa(@RequestParam Long id) throws JRException {
		byte[] relatorio = vendaService.relatorioPedidoRemessa(id);
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE).body(relatorio);
	}
	/**
	 * Relatorio de Itens não entregues na data.
	 * @return
	 * @throws Exception 
	 */
	@GetMapping("/relatorio/itens-nao-entregues")
	public ResponseEntity<byte[]> relatorioItensNaoEntregues(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate de, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate ate) throws Exception {
		byte[] relatorio = vendaService.relatorioItensNaoentregues(de, ate);
		if(relatorio != null) {
			return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE).body(relatorio);
		}
		return null;
	}
	
	/**
	 * Retorna total das Venda
	 */
	@GetMapping("/total")
	@ApiOperation(value = "Retorna total das venas")
	public BigDecimal totalVendas(VendaFilter vendaFilter) {
		return vendaService.totalVendas(vendaFilter);
	}

}
