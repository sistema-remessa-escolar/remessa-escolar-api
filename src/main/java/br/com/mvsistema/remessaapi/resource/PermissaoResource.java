package br.com.mvsistema.remessaapi.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.remessaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.remessaapi.filter.PermissaoFilter;
import br.com.mvsistema.remessaapi.model.Permissao;
import br.com.mvsistema.remessaapi.model.dto.PermissaoDTO;
import br.com.mvsistema.remessaapi.service.PermissaoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/permissao")
@Api(value = "API Rest PermissaoResource")
public class PermissaoResource {
	
	@Autowired
	private PermissaoService permissaoService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	/**
	 * Pesquisar Permissao
	 */
	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Permissao")
	public Page<Permissao> search(PermissaoFilter permissaoFilter, Pageable pageable) {
		return permissaoService.filter(permissaoFilter, pageable);
	}
	
	@GetMapping("/pai-filho")
	@ApiOperation(value = "Retorna uma Lista de Permissao pai e filho")
	public List<PermissaoDTO> listaPermissaoPaiAndFilho(){
		return permissaoService.listaPermissaoPaiAndFilho();
	}
	
	/**
	 * Buscar pelo codigo o Permissao
	 */
	@GetMapping("/{code}")
	@ApiOperation(value = "Retorna um Permissao pelo Codigo")
	public ResponseEntity<Permissao> buscarPeloCodigo(@PathVariable Long code) {
		Permissao permissao = permissaoService.buscarPermissaoPeloCodigo(code);
		return permissao != null ? ResponseEntity.ok(permissao) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar Permissao
	 */
	@PostMapping
	@ApiOperation(value = "criar um Permissao")
	public ResponseEntity<Permissao> criar(@Valid @RequestBody Permissao permissao, HttpServletResponse response) {
		Permissao permissaoSalvo = permissaoService.save(permissao);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, permissaoSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(permissaoSalvo);
	}
	
	/**
	 * Atualizar Permissao
	 */
	@PutMapping("/{codigo}")
	@ApiOperation(value = "Atualizar um Permissao")
	public ResponseEntity<Permissao> atualizar(@PathVariable Long codigo, @Valid @RequestBody Permissao permissao) {
		Permissao permissaoSalva = permissaoService.atualizar(codigo, permissao);
		return ResponseEntity.ok(permissaoSalva);
	}
	

	/**
	 * Deletar Permissao
	 */
	@DeleteMapping("/{code}")
	@ApiOperation(value = "Deletar um Permissao")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		permissaoService.delete(code);
	}

}
