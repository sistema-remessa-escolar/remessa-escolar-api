package br.com.mvsistema.remessaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.remessaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.remessaapi.filter.CondicaoPagamentoFilter;
import br.com.mvsistema.remessaapi.model.CondicaoPagamento;
import br.com.mvsistema.remessaapi.service.CondicaoPagamentoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/condicaoPagamento")
@Api(value = "API Rest CondicaoPagamentoResource")
public class CondicaoPagamentoResource {
	
	@Autowired
	private CondicaoPagamentoService condicaoPagamentoService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	/**
	 * Pesquisar CondicaoPagamento
	 */
	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de CondicaoPagamento")
	public Page<CondicaoPagamento> search(CondicaoPagamentoFilter condicaoPagamentoFilter, Pageable pageable) {
		return condicaoPagamentoService.filter(condicaoPagamentoFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o CondicaoPagamento
	 */
	@GetMapping("/{code}")
	@ApiOperation(value = "Retorna um CondicaoPagamento pelo Codigo")
	public ResponseEntity<CondicaoPagamento> buscarPeloCodigo(@PathVariable Long code) {
		CondicaoPagamento condicaoPagamento = condicaoPagamentoService.buscarCondicaoPagamentoPeloCodigo(code);
		return condicaoPagamento != null ? ResponseEntity.ok(condicaoPagamento) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar CondicaoPagamento
	 */
	@PostMapping
	@ApiOperation(value = "criar um CondicaoPagamento")
	public ResponseEntity<CondicaoPagamento> criar(@Valid @RequestBody CondicaoPagamento condicaoPagamento, HttpServletResponse response) {
		CondicaoPagamento condicaoPagamentoSalvo = condicaoPagamentoService.save(condicaoPagamento);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, condicaoPagamentoSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(condicaoPagamentoSalvo);
	}
	
	/**
	 * Atualizar CondicaoPagamento
	 */
	@PutMapping("/{codigo}")
	@ApiOperation(value = "Atualizar um CondicaoPagamento")
	public ResponseEntity<CondicaoPagamento> atualizar(@PathVariable Long codigo, @Valid @RequestBody CondicaoPagamento condicaoPagamento) {
		CondicaoPagamento condicaoPagamentoSalva = condicaoPagamentoService.atualizar(codigo, condicaoPagamento);
		return ResponseEntity.ok(condicaoPagamentoSalva);
	}
	

	/**
	 * Deletar CondicaoPagamento
	 */
	@DeleteMapping("/{code}")
	@ApiOperation(value = "Deletar um CondicaoPagamento")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		condicaoPagamentoService.delete(code);
	}

}
