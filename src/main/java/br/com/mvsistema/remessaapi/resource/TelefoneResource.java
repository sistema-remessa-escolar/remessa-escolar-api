package br.com.mvsistema.remessaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.remessaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.remessaapi.filter.TelefoneFilter;
import br.com.mvsistema.remessaapi.model.Telefone;
import br.com.mvsistema.remessaapi.service.TelefoneService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/telefone")
@Api(value = "API Rest TelefoneResource")
public class TelefoneResource {
	
	@Autowired
	private TelefoneService telefoneService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	/**
	 * Pesquisar Telefone
	 */
	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Telefone")
	public Page<Telefone> search(TelefoneFilter telefoneFilter, Pageable pageable) {
		return telefoneService.filter(telefoneFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o Telefone
	 */
	@GetMapping("/{code}")
	@ApiOperation(value = "Retorna um Telefone pelo Codigo")
	public ResponseEntity<Telefone> buscarPeloCodigo(@PathVariable Long code) {
		Telefone telefone = telefoneService.buscarTelefonePeloCodigo(code);
		return telefone != null ? ResponseEntity.ok(telefone) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar Telefone
	 */
	@PostMapping
	@ApiOperation(value = "criar um Telefone")
	public ResponseEntity<Telefone> criar(@Valid @RequestBody Telefone telefone, HttpServletResponse response) {
		Telefone telefoneSalvo = telefoneService.save(telefone);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, telefoneSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(telefoneSalvo);
	}
	
	/**
	 * Atualizar Telefone
	 */
	@PutMapping("/{codigo}")
	@ApiOperation(value = "Atualizar um Telefone")
	public ResponseEntity<Telefone> atualizar(@PathVariable Long codigo, @Valid @RequestBody Telefone telefone) {
		Telefone telefoneSalva = telefoneService.atualizar(codigo, telefone);
		return ResponseEntity.ok(telefoneSalva);
	}
	

	/**
	 * Deletar Telefone
	 */
	@DeleteMapping("/{code}")
	@ApiOperation(value = "Deletar um Telefone")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		telefoneService.delete(code);
	}

}
