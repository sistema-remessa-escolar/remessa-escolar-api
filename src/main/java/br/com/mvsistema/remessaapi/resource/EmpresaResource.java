package br.com.mvsistema.remessaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.remessaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.remessaapi.filter.EmpresaFilter;
import br.com.mvsistema.remessaapi.model.Empresa;
import br.com.mvsistema.remessaapi.service.EmpresaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/empresa")
@Api(value = "API Rest EmpresaResource")
public class EmpresaResource {
	
	@Autowired
	private EmpresaService empresaService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	/**
	 * Pesquisar Empresa
	 */
	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Empresa")
	public Page<Empresa> search(EmpresaFilter empresaFilter, Pageable pageable) {
		return empresaService.filter(empresaFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o Empresa
	 */
	@GetMapping("/{code}")
	@ApiOperation(value = "Retorna um Empresa pelo Codigo")
	public ResponseEntity<Empresa> buscarPeloCodigo(@PathVariable Long code) {
		Empresa empresa = empresaService.buscarEmpresaPeloCodigo(code);
		return empresa != null ? ResponseEntity.ok(empresa) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar Empresa
	 */
	@PostMapping
	@ApiOperation(value = "criar um Empresa")
	public ResponseEntity<Empresa> criar(@Valid @RequestBody Empresa empresa, HttpServletResponse response) {
		Empresa empresaSalvo = empresaService.save(empresa);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, empresaSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(empresaSalvo);
	}
	
	/**
	 * Atualizar Empresa
	 */
	@PutMapping("/{codigo}")
	@ApiOperation(value = "Atualizar um Empresa")
	public ResponseEntity<Empresa> atualizar(@PathVariable Long codigo, @Valid @RequestBody Empresa empresa) {
		Empresa empresaSalva = empresaService.atualizar(codigo, empresa);
		return ResponseEntity.ok(empresaSalva);
	}
	

	/**
	 * Deletar Empresa
	 */
	@DeleteMapping("/{code}")
	@ApiOperation(value = "Deletar um Empresa")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		empresaService.delete(code);
	}

}
