package br.com.mvsistema.remessaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import br.com.mvsistema.remessaapi.filter.ProdutoFilter;
import br.com.mvsistema.remessaapi.model.Produto;



public interface ProdutoHelper {

	public Page<Produto> filtrar(ProdutoFilter produtoFilter, Pageable pageable);

}
