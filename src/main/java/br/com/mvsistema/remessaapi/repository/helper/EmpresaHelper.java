package br.com.mvsistema.remessaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import br.com.mvsistema.remessaapi.filter.EmpresaFilter;
import br.com.mvsistema.remessaapi.model.Empresa;



public interface EmpresaHelper {

	public Page<Empresa> filtrar(EmpresaFilter empresaFilter, Pageable pageable);

}
