package br.com.mvsistema.remessaapi.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.remessaapi.model.Item;
import br.com.mvsistema.remessaapi.repository.helper.ItemHelper;



@Repository
public interface ItemRepository extends JpaRepository<Item, Long>,ItemHelper {

	

}

