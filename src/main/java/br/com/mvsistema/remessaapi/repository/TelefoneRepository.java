package br.com.mvsistema.remessaapi.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.remessaapi.model.Telefone;
import br.com.mvsistema.remessaapi.repository.helper.TelefoneHelper;



@Repository
public interface TelefoneRepository extends JpaRepository<Telefone, Long>,TelefoneHelper {

	

}

