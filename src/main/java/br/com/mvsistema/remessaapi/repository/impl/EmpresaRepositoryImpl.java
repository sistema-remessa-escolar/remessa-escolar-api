package br.com.mvsistema.remessaapi.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.mvsistema.remessaapi.repository.helper.EmpresaHelper;
import br.com.mvsistema.remessaapi.filter.EmpresaFilter;
import br.com.mvsistema.remessaapi.model.Empresa;

public class EmpresaRepositoryImpl implements EmpresaHelper {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Empresa> filtrar(EmpresaFilter empresaFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Empresa> criteria = builder.createQuery(Empresa.class);
		Root<Empresa> root = criteria.from(Empresa.class);

		Predicate[] predicates = criarRestrincoes(empresaFilter, builder, root);
		criteria.where(predicates);

		TypedQuery<Empresa> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);

		return new PageImpl<>(query.getResultList(), pageable, total(empresaFilter));
	}

	private Predicate[] criarRestrincoes(EmpresaFilter empresaFilter, CriteriaBuilder builder, Root<Empresa> root) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		if (empresaFilter != null) {

			if (empresaFilter.getId() != null) {
				predicates.add(builder.equal(root.get("id"), empresaFilter.getId()));
			}

			if (!StringUtils.isEmpty(empresaFilter.getRazaoSocial())) {
				predicates.add(builder.like(builder.lower(root.get("razaoSocial")),
						"%" + empresaFilter.getRazaoSocial().toLowerCase() + "%"));
			}

			if (!StringUtils.isEmpty(empresaFilter.getNomeFantasia())) {
				predicates.add(builder.like(builder.lower(root.get("nomeFantasia")),
						"%" + empresaFilter.getNomeFantasia().toLowerCase() + "%"));
			}

			if (!StringUtils.isEmpty(empresaFilter.getEmail())) {
				predicates.add(builder.like(builder.lower(root.get("email")),
						"%" + empresaFilter.getEmail().toLowerCase() + "%"));
			}

			if (!StringUtils.isEmpty(empresaFilter.getCnpj())) {
				predicates.add(builder.like(builder.lower(root.get("cnpj")),
						"%" + empresaFilter.getCnpj().toLowerCase() + "%"));
			}

			if (!StringUtils.isEmpty(empresaFilter.getIe())) {
				predicates.add(
						builder.like(builder.lower(root.get("ie")), "%" + empresaFilter.getIe().toLowerCase() + "%"));
			}

			if (empresaFilter.getEndereco() != null) {
				predicates.add(builder.equal(root.get("endereco"), empresaFilter.getEndereco()));
			}

		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}

	private Long total(EmpresaFilter empresaFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Empresa> root = criteria.from(Empresa.class);

		Predicate[] predicates = criarRestrincoes(empresaFilter, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
