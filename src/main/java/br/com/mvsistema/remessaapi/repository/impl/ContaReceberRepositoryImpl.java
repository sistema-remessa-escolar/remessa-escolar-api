package br.com.mvsistema.remessaapi.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.mvsistema.remessaapi.repository.helper.ContaReceberHelper;
import br.com.mvsistema.remessaapi.filter.ContaReceberFilter;
import br.com.mvsistema.remessaapi.model.ContaReceber;


public class ContaReceberRepositoryImpl implements ContaReceberHelper {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<ContaReceber> filtrar(ContaReceberFilter  contaReceberFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ContaReceber> criteria = builder.createQuery(ContaReceber.class);
		Root<ContaReceber> root = criteria.from(ContaReceber.class);
		
		Predicate[] predicates = criarRestrincoes(contaReceberFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<ContaReceber> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(contaReceberFilter));
	}
	
	
	private Predicate[] criarRestrincoes(ContaReceberFilter  contaReceberFilter, CriteriaBuilder builder, Root<ContaReceber> root) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		if (contaReceberFilter != null) {
			
                      if (contaReceberFilter.getId() != null) {
				predicates.add(builder.equal(root.get("id"), contaReceberFilter.getId()));
			}

            if (contaReceberFilter.getValorTotalConta() != null) {
				predicates.add(builder.equal(root.get("valorTotalConta"), contaReceberFilter.getValorTotalConta()));
			}

            if (contaReceberFilter.getDevedor() != null) {
				predicates.add(builder.equal(root.get("devedor"), contaReceberFilter.getDevedor()));
			}



		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(ContaReceberFilter  contaReceberFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<ContaReceber> root = criteria.from(ContaReceber.class);
		
		Predicate[] predicates = criarRestrincoes(contaReceberFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}


	@Override
	public BigDecimal totalRecebimentos(ContaReceberFilter contaReceberFilter) {
		BigDecimal total = BigDecimal.ZERO;
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ContaReceber> criteria = builder.createQuery(ContaReceber.class);
		Root<ContaReceber> root = criteria.from(ContaReceber.class);
		
		Predicate[] predicates = criarRestrincoes(contaReceberFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<ContaReceber> query = manager.createQuery(criteria);

		List<ContaReceber> contas = query.getResultList();
		
		for(ContaReceber cr : contas) {
			total = total.add(cr.getValorParcela());
		}
		 return total; 
	}

}
