package br.com.mvsistema.remessaapi.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.remessaapi.model.ContaReceber;
import br.com.mvsistema.remessaapi.repository.helper.ContaReceberHelper;



@Repository
public interface ContaReceberRepository extends JpaRepository<ContaReceber, Long>,ContaReceberHelper {

	

}

