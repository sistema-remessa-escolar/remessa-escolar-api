package br.com.mvsistema.remessaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import br.com.mvsistema.remessaapi.filter.UsuarioFilter;
import br.com.mvsistema.remessaapi.model.Usuario;



public interface UsuarioHelper {

	public Page<Usuario> filtrar(UsuarioFilter usuarioFilter, Pageable pageable);

}
