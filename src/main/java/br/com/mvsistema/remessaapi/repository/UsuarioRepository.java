package br.com.mvsistema.remessaapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.remessaapi.model.Usuario;
import br.com.mvsistema.remessaapi.repository.helper.UsuarioHelper;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>, UsuarioHelper {

	Optional<Usuario> findByCpf(String cpf);
}
