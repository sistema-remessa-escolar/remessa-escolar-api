package br.com.mvsistema.remessaapi.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.mvsistema.remessaapi.repository.helper.EscolaHelper;
import br.com.mvsistema.remessaapi.filter.EscolaFilter;
import br.com.mvsistema.remessaapi.model.Escola;


public class EscolaRepositoryImpl implements EscolaHelper {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Escola> filtrar(EscolaFilter  escolaFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Escola> criteria = builder.createQuery(Escola.class);
		Root<Escola> root = criteria.from(Escola.class);
		
		Predicate[] predicates = criarRestrincoes(escolaFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Escola> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(escolaFilter));
	}
	
	
	private Predicate[] criarRestrincoes(EscolaFilter  escolaFilter, CriteriaBuilder builder, Root<Escola> root) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		if (escolaFilter != null) {
			
                      if (escolaFilter.getId() != null) {
				predicates.add(builder.equal(root.get("id"), escolaFilter.getId()));
			}

           if (!StringUtils.isEmpty(escolaFilter.getRazaoSocial())) {				
				predicates.add(builder.like(builder.lower(root.get("razaoSocial")),"%" + escolaFilter.getRazaoSocial().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(escolaFilter.getNomeFantasia())) {				
				predicates.add(builder.like(builder.lower(root.get("nomeFantasia")),"%" + escolaFilter.getNomeFantasia().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(escolaFilter.getEmail())) {				
				predicates.add(builder.like(builder.lower(root.get("email")),"%" + escolaFilter.getEmail().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(escolaFilter.getCnpj())) {				
				predicates.add(builder.like(builder.lower(root.get("cnpj")),"%" + escolaFilter.getCnpj().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(escolaFilter.getIe())) {				
				predicates.add(builder.like(builder.lower(root.get("ie")),"%" + escolaFilter.getIe().toLowerCase() + "%"));
			}
			

//predicates.add(builder.equal(root.get("bloqueado"), escolaFilter.isBloqueado()));
//			
//
//predicates.add(builder.equal(root.get("ativo"), escolaFilter.isAtivo()));
			

            if (escolaFilter.getEndereco() != null) {
				predicates.add(builder.equal(root.get("endereco"), escolaFilter.getEndereco()));
			}



		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(EscolaFilter  escolaFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Escola> root = criteria.from(Escola.class);
		
		Predicate[] predicates = criarRestrincoes(escolaFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
