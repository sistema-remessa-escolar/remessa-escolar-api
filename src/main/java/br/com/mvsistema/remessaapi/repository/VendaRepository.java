package br.com.mvsistema.remessaapi.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.remessaapi.model.Item;
import br.com.mvsistema.remessaapi.model.Venda;
import br.com.mvsistema.remessaapi.repository.helper.VendaHelper;

@Repository
public interface VendaRepository extends JpaRepository<Venda, Long>, VendaHelper {

	@Query(value = "Select iv From Item iv where iv.dataEntrega between :de and :ate and iv.dataEntrega is not null")
	public List<Item> itensNaoEntreguesNaData(@Param("de") LocalDate de, @Param("ate") LocalDate ate);

}
