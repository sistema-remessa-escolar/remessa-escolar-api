package br.com.mvsistema.remessaapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.remessaapi.model.Produto;
import br.com.mvsistema.remessaapi.repository.helper.ProdutoHelper;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long>, ProdutoHelper {

}
