package br.com.mvsistema.remessaapi.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.remessaapi.model.Escola;
import br.com.mvsistema.remessaapi.repository.helper.EscolaHelper;



@Repository
public interface EscolaRepository extends JpaRepository<Escola, Long>,EscolaHelper {

	

}

