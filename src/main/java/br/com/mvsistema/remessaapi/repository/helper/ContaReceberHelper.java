package br.com.mvsistema.remessaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;

import br.com.mvsistema.remessaapi.filter.ContaReceberFilter;
import br.com.mvsistema.remessaapi.filter.VendaFilter;
import br.com.mvsistema.remessaapi.model.ContaReceber;



public interface ContaReceberHelper {

	public Page<ContaReceber> filtrar(ContaReceberFilter contaReceberFilter, Pageable pageable);
	public BigDecimal totalRecebimentos(ContaReceberFilter contaReceberFilter);

}
