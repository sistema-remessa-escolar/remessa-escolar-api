package br.com.mvsistema.remessaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import br.com.mvsistema.remessaapi.filter.PermissaoFilter;
import br.com.mvsistema.remessaapi.model.Permissao;



public interface PermissaoHelper {

	public Page<Permissao> filtrar(PermissaoFilter permissaoFilter, Pageable pageable);

}
