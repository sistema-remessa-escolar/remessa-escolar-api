package br.com.mvsistema.remessaapi.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.mvsistema.remessaapi.repository.helper.UsuarioHelper;
import br.com.mvsistema.remessaapi.filter.UsuarioFilter;
import br.com.mvsistema.remessaapi.model.Usuario;


public class UsuarioRepositoryImpl implements UsuarioHelper {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Usuario> filtrar(UsuarioFilter  usuarioFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Usuario> criteria = builder.createQuery(Usuario.class);
		Root<Usuario> root = criteria.from(Usuario.class);
		
		Predicate[] predicates = criarRestrincoes(usuarioFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Usuario> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(usuarioFilter));
	}
	
	
	private Predicate[] criarRestrincoes(UsuarioFilter  usuarioFilter, CriteriaBuilder builder, Root<Usuario> root) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		if (usuarioFilter != null) {
			
                      if (usuarioFilter.getId() != null) {
				predicates.add(builder.equal(root.get("id"), usuarioFilter.getId()));
			}

           if (!StringUtils.isEmpty(usuarioFilter.getCpf())) {				
				predicates.add(builder.like(builder.lower(root.get("cpf")),"%" + usuarioFilter.getCpf().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(usuarioFilter.getNome())) {				
				predicates.add(builder.like(builder.lower(root.get("nome")),"%" + usuarioFilter.getNome().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(usuarioFilter.getEmail())) {				
				predicates.add(builder.like(builder.lower(root.get("email")),"%" + usuarioFilter.getEmail().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(usuarioFilter.getSenha())) {				
				predicates.add(builder.like(builder.lower(root.get("senha")),"%" + usuarioFilter.getSenha().toLowerCase() + "%"));
			}
			

            if (usuarioFilter.getPermissoes() != null) {
				predicates.add(builder.equal(root.get("permissoes"), usuarioFilter.getPermissoes()));
			}

            if (usuarioFilter.getGrupos() != null) {
				predicates.add(builder.equal(root.get("grupos"), usuarioFilter.getGrupos()));
			}
            
            predicates.add(builder.notEqual(root.get("id"), 1));
            
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(UsuarioFilter  usuarioFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Usuario> root = criteria.from(Usuario.class);
		
		Predicate[] predicates = criarRestrincoes(usuarioFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
