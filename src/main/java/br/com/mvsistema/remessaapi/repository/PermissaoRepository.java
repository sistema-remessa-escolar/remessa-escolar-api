package br.com.mvsistema.remessaapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.remessaapi.model.Permissao;
import br.com.mvsistema.remessaapi.repository.helper.PermissaoHelper;

@Repository
public interface PermissaoRepository extends JpaRepository<Permissao, Long>, PermissaoHelper {

	@Query(value = "Select p From Permissao p where p.permissaoPai is null")
	List<Permissao> listaPermisssaoPai();
	
	@Query(value = "Select p From Permissao p where p.permissaoPai = :idPermissaoPai")
	List<Permissao> listaPermisssaoFilho(@Param("idPermissaoPai") Permissao idPermissaoPai);
}
