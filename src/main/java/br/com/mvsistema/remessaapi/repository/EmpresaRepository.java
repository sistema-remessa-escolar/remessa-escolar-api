package br.com.mvsistema.remessaapi.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.remessaapi.model.Empresa;
import br.com.mvsistema.remessaapi.repository.helper.EmpresaHelper;



@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Long>,EmpresaHelper {

	

}

