package br.com.mvsistema.remessaapi.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.remessaapi.model.CondicaoPagamento;
import br.com.mvsistema.remessaapi.repository.helper.CondicaoPagamentoHelper;



@Repository
public interface CondicaoPagamentoRepository extends JpaRepository<CondicaoPagamento, Long>,CondicaoPagamentoHelper {

	

}

