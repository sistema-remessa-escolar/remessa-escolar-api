package br.com.mvsistema.remessaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import br.com.mvsistema.remessaapi.filter.EscolaFilter;
import br.com.mvsistema.remessaapi.model.Escola;



public interface EscolaHelper {

	public Page<Escola> filtrar(EscolaFilter escolaFilter, Pageable pageable);

}
