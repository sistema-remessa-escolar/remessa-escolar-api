package br.com.mvsistema.remessaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;

import br.com.mvsistema.remessaapi.filter.VendaFilter;
import br.com.mvsistema.remessaapi.model.Venda;
import net.sf.jasperreports.engine.JRException;



public interface VendaHelper {

	public Page<Venda> filtrar(VendaFilter vendaFilter, Pageable pageable);
	public BigDecimal totalVenas(VendaFilter vendaFilter);
	
}
