package br.com.mvsistema.remessaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import br.com.mvsistema.remessaapi.filter.CondicaoPagamentoFilter;
import br.com.mvsistema.remessaapi.model.CondicaoPagamento;



public interface CondicaoPagamentoHelper {

	public Page<CondicaoPagamento> filtrar(CondicaoPagamentoFilter condicaoPagamentoFilter, Pageable pageable);

}
