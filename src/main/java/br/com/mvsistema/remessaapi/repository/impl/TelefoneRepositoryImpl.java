package br.com.mvsistema.remessaapi.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.mvsistema.remessaapi.repository.helper.TelefoneHelper;
import br.com.mvsistema.remessaapi.filter.TelefoneFilter;
import br.com.mvsistema.remessaapi.model.Telefone;


public class TelefoneRepositoryImpl implements TelefoneHelper {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Telefone> filtrar(TelefoneFilter  telefoneFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Telefone> criteria = builder.createQuery(Telefone.class);
		Root<Telefone> root = criteria.from(Telefone.class);
		
		Predicate[] predicates = criarRestrincoes(telefoneFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Telefone> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(telefoneFilter));
	}
	
	
	private Predicate[] criarRestrincoes(TelefoneFilter  telefoneFilter, CriteriaBuilder builder, Root<Telefone> root) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		if (telefoneFilter != null) {
			
                      if (telefoneFilter.getId() != null) {
				predicates.add(builder.equal(root.get("id"), telefoneFilter.getId()));
			}

           if (!StringUtils.isEmpty(telefoneFilter.getNumero())) {				
				predicates.add(builder.like(builder.lower(root.get("numero")),"%" + telefoneFilter.getNumero().toLowerCase() + "%"));
			}
			



		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(TelefoneFilter  telefoneFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Telefone> root = criteria.from(Telefone.class);
		
		Predicate[] predicates = criarRestrincoes(telefoneFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
