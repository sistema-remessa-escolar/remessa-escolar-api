package br.com.mvsistema.remessaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import br.com.mvsistema.remessaapi.filter.TelefoneFilter;
import br.com.mvsistema.remessaapi.model.Telefone;



public interface TelefoneHelper {

	public Page<Telefone> filtrar(TelefoneFilter telefoneFilter, Pageable pageable);

}
