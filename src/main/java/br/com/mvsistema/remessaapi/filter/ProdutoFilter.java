package br.com.mvsistema.remessaapi.filter;

import java.math.BigDecimal;


public class ProdutoFilter {

private Long id;
private String nomeProduto;
private BigDecimal valor;
private boolean ativo;


public Long getId() {
  return this.id;
}
public void setId(Long id) {
  this.id = id;
}
public String getNomeProduto() {
  return this.nomeProduto;
}
public void setNomeProduto(String nomeProduto) {
  this.nomeProduto = nomeProduto;
}
public BigDecimal getValor() {
  return this.valor;
}
public void setValor(BigDecimal valor) {
  this.valor = valor;
}
public boolean isAtivo() {
  return this.ativo;
}
public void setAtivo(boolean ativo) {
  this.ativo = ativo;
}



}
