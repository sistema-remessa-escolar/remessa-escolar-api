package br.com.mvsistema.remessaapi.filter;

import java.time.LocalDate;
import br.com.mvsistema.remessaapi.model.Escola;
import br.com.mvsistema.remessaapi.model.Empresa;
import java.util.List;
import java.math.BigDecimal;
import java.math.BigDecimal;
import br.com.mvsistema.remessaapi.model.TipoRemessa;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VendaFilter {

	private Long id;
	private String numeroDoPedido;
	private LocalDate cadastro;
	private Long escola;
	private Long empresa;
	private BigDecimal frete;
	private BigDecimal total;
	private TipoRemessa tipoRemessa;

}
