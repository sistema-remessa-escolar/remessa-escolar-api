package br.com.mvsistema.remessaapi.filter;

import br.com.mvsistema.remessaapi.model.Venda;
import br.com.mvsistema.remessaapi.model.Produto;
import java.math.BigDecimal;


public class ItemFilter {

private Long id;
private Venda venda;
private Produto produto;
private BigDecimal quantidade;


public Long getId() {
  return this.id;
}
public void setId(Long id) {
  this.id = id;
}
public Venda getVenda() {
  return this.venda;
}
public void setVenda(Venda venda) {
  this.venda = venda;
}
public Produto getProduto() {
  return this.produto;
}
public void setProduto(Produto produto) {
  this.produto = produto;
}
public BigDecimal getQuantidade() {
  return this.quantidade;
}
public void setQuantidade(BigDecimal quantidade) {
  this.quantidade = quantidade;
}



}
