package br.com.mvsistema.remessaapi.filter;

import java.util.List;
import java.math.BigDecimal;
import br.com.mvsistema.remessaapi.model.Escola;


public class ContaReceberFilter {

private Long id;
private BigDecimal valorTotalConta;
private Escola devedor;


public Long getId() {
  return this.id;
}
public void setId(Long id) {
  this.id = id;
}
public BigDecimal getValorTotalConta() {
  return this.valorTotalConta;
}
public void setValorTotalConta(BigDecimal valorTotalConta) {
  this.valorTotalConta = valorTotalConta;
}
public Escola getDevedor() {
  return this.devedor;
}
public void setDevedor(Escola devedor) {
  this.devedor = devedor;
}



}
