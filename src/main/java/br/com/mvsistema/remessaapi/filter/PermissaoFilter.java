package br.com.mvsistema.remessaapi.filter;

import java.util.List;
import java.util.List;


public class PermissaoFilter {

private Long id;
private String descricao;
private List usuarios;
private List grupos;


public Long getId() {
  return this.id;
}
public void setId(Long id) {
  this.id = id;
}
public String getDescricao() {
  return this.descricao;
}
public void setDescricao(String descricao) {
  this.descricao = descricao;
}
public List getUsuarios() {
  return this.usuarios;
}
public void setUsuarios(List usuarios) {
  this.usuarios = usuarios;
}
public List getGrupos() {
  return this.grupos;
}
public void setGrupos(List grupos) {
  this.grupos = grupos;
}



}
