package br.com.mvsistema.remessaapi.filter;

import br.com.mvsistema.remessaapi.model.TipoCondicaoPagamento;


public class CondicaoPagamentoFilter {

private Long id;
private TipoCondicaoPagamento tipoCondicaoPagamento;
private String descricao;
private String formula;


public Long getId() {
  return this.id;
}
public void setId(Long id) {
  this.id = id;
}
public TipoCondicaoPagamento getTipoCondicaoPagamento() {
  return this.tipoCondicaoPagamento;
}
public void setTipoCondicaoPagamento(TipoCondicaoPagamento tipoCondicaoPagamento) {
  this.tipoCondicaoPagamento = tipoCondicaoPagamento;
}
public String getDescricao() {
  return this.descricao;
}
public void setDescricao(String descricao) {
  this.descricao = descricao;
}
public String getFormula() {
  return this.formula;
}
public void setFormula(String formula) {
  this.formula = formula;
}



}
