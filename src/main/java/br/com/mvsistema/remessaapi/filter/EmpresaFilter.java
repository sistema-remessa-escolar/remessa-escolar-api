package br.com.mvsistema.remessaapi.filter;

import br.com.mvsistema.remessaapi.model.Endereco;
import java.util.List;


public class EmpresaFilter {

private Long id;
private String razaoSocial;
private String nomeFantasia;
private String email;
private String cnpj;
private String ie;
private Endereco endereco;


public Long getId() {
  return this.id;
}
public void setId(Long id) {
  this.id = id;
}
public String getRazaoSocial() {
  return this.razaoSocial;
}
public void setRazaoSocial(String razaoSocial) {
  this.razaoSocial = razaoSocial;
}
public String getNomeFantasia() {
  return this.nomeFantasia;
}
public void setNomeFantasia(String nomeFantasia) {
  this.nomeFantasia = nomeFantasia;
}
public String getEmail() {
  return this.email;
}
public void setEmail(String email) {
  this.email = email;
}
public String getCnpj() {
  return this.cnpj;
}
public void setCnpj(String cnpj) {
  this.cnpj = cnpj;
}
public String getIe() {
  return this.ie;
}
public void setIe(String ie) {
  this.ie = ie;
}
public Endereco getEndereco() {
  return this.endereco;
}
public void setEndereco(Endereco endereco) {
  this.endereco = endereco;
}



}
