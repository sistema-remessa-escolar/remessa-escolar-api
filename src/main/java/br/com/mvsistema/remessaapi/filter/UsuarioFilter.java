package br.com.mvsistema.remessaapi.filter;

import java.util.List;
import java.util.Set;


public class UsuarioFilter {

private Long id;
private String cpf;
private String nome;
private String email;
private String senha;
private List permissoes;
private Set grupos;


public Long getId() {
  return this.id;
}
public void setId(Long id) {
  this.id = id;
}
public String getCpf() {
  return this.cpf;
}
public void setCpf(String cpf) {
  this.cpf = cpf;
}
public String getNome() {
  return this.nome;
}
public void setNome(String nome) {
  this.nome = nome;
}
public String getEmail() {
  return this.email;
}
public void setEmail(String email) {
  this.email = email;
}
public String getSenha() {
  return this.senha;
}
public void setSenha(String senha) {
  this.senha = senha;
}
public List getPermissoes() {
  return this.permissoes;
}
public void setPermissoes(List permissoes) {
  this.permissoes = permissoes;
}
public Set getGrupos() {
  return this.grupos;
}
public void setGrupos(Set grupos) {
  this.grupos = grupos;
}



}
