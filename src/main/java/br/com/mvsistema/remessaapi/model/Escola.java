package br.com.mvsistema.remessaapi.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CNPJ;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "escola")
public class Escola {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@NotEmpty(message = "Informe a Razão Social")
	@Column(name = "razao_social", length = 60, nullable = false)
	private String razaoSocial;
	
	@Column(name = "nome_fantasia", length = 60, nullable = false)
	private String nomeFantasia;
	
	@Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="invalido ou campo em branco")
	private String email;
	
	@NotEmpty(message = "Informe o CNPJ")
	@CNPJ
	private String cnpj;
	
	@NotEmpty(message = "Informe a IE")
	private String ie;
	
	private boolean bloqueado;
	
	private boolean ativo;
	
	private String diretor;
	
	@Embedded
	private Endereco endereco = new Endereco();
	
	@OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL) 
	@JoinTable(name = "escola_telefone", joinColumns = @JoinColumn(name = "id_escola"), inverseJoinColumns = @JoinColumn(name = "id_telefone"))
	private List<Telefone> telefonesEsc = new ArrayList<Telefone>();
}
