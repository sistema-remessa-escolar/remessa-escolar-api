package br.com.mvsistema.remessaapi.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "venda")
public class Venda {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@Getter
	@Setter
	private Long id;
	
	@Getter
	@Setter
	private String numeroDoPedido;

	@Getter
	@Setter
	private LocalDate cadastro;
	
	@Getter
	@Setter
	private LocalDate dataEntrega;

	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "escola_id", nullable = false, foreignKey = @ForeignKey(name = "fk_escola_venda"))
	private Escola escola;

	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "empresa_id", nullable = false, foreignKey = @ForeignKey(name = "fk_empresa_venda"))
	private Empresa empresa;
	
	@JsonIgnoreProperties("venda")
	@OneToMany(mappedBy = "venda", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval=true)
	@Fetch(FetchMode.SELECT)
	private List<ContaReceber> contasReceber = new ArrayList<ContaReceber>();

	@JsonIgnoreProperties("venda")
	@Valid
	@OneToMany(mappedBy = "venda", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval=true)
	@Fetch(FetchMode.SELECT)
	private List<Item> itens = new ArrayList<Item>();

	@Getter
	@Setter
	@Column(precision = 19, scale = 6)
	private BigDecimal frete = BigDecimal.ZERO;
	
	@Getter
	@Setter
	@Column(precision = 19, scale = 6)
	private BigDecimal total = BigDecimal.ZERO;

	@Getter
	@Setter
	@Enumerated(EnumType.STRING)
	private TipoRemessa tipoRemessa;
	
	@Getter
	@Setter
	@Transient
	private BigDecimal totalvendas;
	
	public List<Item> getItens() {
		return itens;
	}
	
	public void setItens(List<Item> itens) {
		/**
		 * Corrigindo o erro: 
		 * HibernateException - A collection with cascade="all-delete-orphan" was no longer referenced by the owning entity instance
		 * limpando a lista e refazendo colocando essa iteração na lista de Itens
		 */
		this.itens.clear();
		
		this.itens.forEach(item -> {
			item.setVenda(null);
		});
		
	    this.itens.addAll(itens);
	}
	
	public List<ContaReceber> getContasReceber() {
		return contasReceber;
	}
	
	public void setContasReceber(List<ContaReceber> contasReceber) {
		/**
		 * Corrigindo o erro: 
		 * HibernateException - A collection with cascade="all-delete-orphan" was no longer referenced by the owning entity instance
		 * limpando a lista e refazendo colocando essa iteração na lista de Itens
		 */
		this.contasReceber.clear();
		
		this.contasReceber.forEach(cr -> {
			cr.setVenda(null);
			cr.setDevedor(null);
		});
		
	    this.contasReceber.addAll(contasReceber);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Venda other = (Venda) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
