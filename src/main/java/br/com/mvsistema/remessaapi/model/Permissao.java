package br.com.mvsistema.remessaapi.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Entity
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "permissao")
public class Permissao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", insertable=false, updatable = false)
	private Long id;
	private String descricao;
	private String role;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "permissao_pai_id")
	private Permissao permissaoPai;
	@JsonIgnoreProperties(value = "permissaoPai")
	@OneToMany(mappedBy = "permissaoPai")
	private List<Permissao> subPermissoes;

}
