package br.com.mvsistema.remessaapi.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "produto")
public class Produto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	private String nomeProduto;

	@Column(precision = 19, scale = 6)
	private BigDecimal valor = BigDecimal.ZERO;
	
	private boolean ativo;
	
	@Enumerated(EnumType.STRING)
	private TipoUnidadeMedida unidadeMedida;
}
