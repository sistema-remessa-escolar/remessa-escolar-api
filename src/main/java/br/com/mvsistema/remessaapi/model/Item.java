package br.com.mvsistema.remessaapi.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "item_venda")
public class Item {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "venda_id", foreignKey = @ForeignKey(name = "fk_venda_item"))
	private Venda venda;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "produto_id",  nullable = false, foreignKey = @ForeignKey(name = "fk_produto_item"))
	private Produto produto;

	@NotNull
	@Column(precision = 19, scale = 6)
	private BigDecimal quantidade = BigDecimal.ZERO;

	@Column(precision = 19, scale = 6)
	private BigDecimal valor = BigDecimal.ZERO;

	@Column(precision = 19, scale = 6)
	private BigDecimal total = BigDecimal.ZERO;
	
	private boolean faltou;
	
	@Column(precision = 19, scale = 6)
	private BigDecimal qtdeFalta = BigDecimal.ZERO;
	
	private LocalDate dataEntrega;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
