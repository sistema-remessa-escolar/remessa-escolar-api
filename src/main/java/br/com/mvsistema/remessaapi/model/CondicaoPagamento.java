package br.com.mvsistema.remessaapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "condicaoPagamento")
public class CondicaoPagamento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Enumerated(EnumType.STRING)
	private TipoCondicaoPagamento tipoCondicaoPagamento;
	
	private String descricao;
	
	@Pattern(regexp = "([0-9]{1,}|[\\/])", message="Permitido numeros e '/'")
	private String formula;
	
}
	