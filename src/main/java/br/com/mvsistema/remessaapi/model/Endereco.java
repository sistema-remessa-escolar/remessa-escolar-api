package br.com.mvsistema.remessaapi.model;

import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Embeddable
public class Endereco {

	private String cep, lougradouro, complemento, 
	   bairro, localidade, uf, ibge, numero;
}
