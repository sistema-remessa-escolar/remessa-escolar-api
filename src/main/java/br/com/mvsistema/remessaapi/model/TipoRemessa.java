package br.com.mvsistema.remessaapi.model;

public enum TipoRemessa {

	MERENDA("Merenda"),
	ROM("Recurso do Orçamento Municipal"),
	PDDE("Programa Dinheito Direto na Escola");
	
	private String descricao;
	
	TipoRemessa(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
