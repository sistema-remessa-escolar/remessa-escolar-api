package br.com.mvsistema.remessaapi.model;

public enum TipoCondicaoPagamento {

	DINHEIRO("Dinheiro"),
	CARTAO("Cartão"),
	CHEQUE("cheque"),
	BOLETO("Boleto"),
	PROMISSORIA("Promissória"),
	CARTEIRA("Carteira");
	
	private String descricao;
	
	TipoCondicaoPagamento(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
