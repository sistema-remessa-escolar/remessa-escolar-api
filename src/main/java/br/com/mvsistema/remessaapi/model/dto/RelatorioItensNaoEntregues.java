package br.com.mvsistema.remessaapi.model.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import br.com.mvsistema.remessaapi.model.Empresa;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RelatorioItensNaoEntregues {
	
	private String nomeCliente;
	private Long codigoProduto;
	private String nomeProduto;
	private BigDecimal quantidadeFalta;
	private LocalDate dataEntrega;
	private Empresa empresa;

}
