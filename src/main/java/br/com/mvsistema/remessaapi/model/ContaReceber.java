package br.com.mvsistema.remessaapi.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "contasReceber")
public class ContaReceber{
	
	@Id
	@Getter
	@Setter
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Getter
	@Setter
	@NotNull
	private String numero;
	
	@Getter
	@Setter
	private String documento;
	
	@Getter
	@Setter
	@NotNull
	@Column(precision = 19, scale = 6)
	private BigDecimal valorParcela = BigDecimal.ZERO;
	
	@Getter
	@Setter
	@Column(precision = 19, scale = 6)
	private BigDecimal valorBaixado = BigDecimal.ZERO;
	
	@Getter
	@Setter
	@Column(precision = 19, scale = 6)
	private BigDecimal acrescimoDesconto = BigDecimal.ZERO;

	@Getter
	@Setter
	@NotNull
	@Enumerated(EnumType.STRING)
	private TipoSituacaoParcela situacao;

	@Getter
	@Setter
	@ManyToOne
	@NotNull
	@JoinColumn(name = "escola_id", nullable = false, foreignKey = @ForeignKey(name = "fk_escola_contasReceber"))
	private Escola devedor;
	
	@Getter
	@Setter
	@NotNull
	@ManyToOne
	@JoinColumn(name = "condicaoPagamento_id", nullable = false, foreignKey = @ForeignKey(name = "fk_condicaoPagamento_contasReceber"))
	private CondicaoPagamento condicaoPagamento;
	
	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "venda_id", nullable = true, foreignKey = @ForeignKey(name = "fk_venda_contasReceber"))
	private Venda venda;
	
	@Getter
	@Setter
	@NotNull
	private LocalDate dataVencimento;
	
	@Getter
	@Setter
	private LocalDate dataPagamento;
	
	@Getter
	@Setter
	@NotNull
	@Enumerated(EnumType.STRING)
	private TipoRemessa tipoRemessa;
	
	@Getter
	@Setter
	private LocalDate cadastro;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaReceber other = (ContaReceber) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
