package br.com.mvsistema.remessaapi.model.dto;

import java.math.BigDecimal;
import java.util.List;

import br.com.mvsistema.remessaapi.model.Item;
import br.com.mvsistema.remessaapi.model.Venda;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RelatorioPedidoRemessa {
	
	private String nomeEmpresa;
	private String enderecoEmpresa;
	private String bairroEmpresa;
	private String cidadeEmpresa;
	private String cepEmpresa;
	private String cnpjEmpresa;
	private String ieEmpresa;
	private String emailEmpresa;
	private String telefoneEmpresa;
	
	private String cliente;
	private String endereco;
	private String programa;
	private String bairro;
	private String cidade;
	private String cep;
	private Long codItem;
	private String nomeItem;
	private BigDecimal qtdeItem;
	private BigDecimal totalItem;
	private BigDecimal totalVenda;
	
	
}
