package br.com.mvsistema.remessaapi.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.mvsistema.remessaapi.model.Permissao;
import lombok.Getter;
import lombok.Setter;

// @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Getter
@Setter
public class PermissaoDTO {
	
	private Permissao permissaoPai;
	@JsonIgnoreProperties(value = "listaPermissaoFilhos")
	private List<Permissao> listaPermissaoFilhos = new ArrayList<Permissao>();

}
