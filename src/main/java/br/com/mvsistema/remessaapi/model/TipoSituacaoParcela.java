package br.com.mvsistema.remessaapi.model;

public enum TipoSituacaoParcela {

	ABERTA("Aberta"), VENCIDA("Vencida"), QUITADA("Quitada"), CANCELADA("Cancelada");

	private String descricao;

	TipoSituacaoParcela(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
