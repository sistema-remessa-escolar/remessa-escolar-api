package br.com.mvsistema.remessaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.mvsistema.remessaapi.config.property.MvSistemaApiProperty;

@SpringBootApplication
@EnableConfigurationProperties(MvSistemaApiProperty.class)
public class RemessaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RemessaApiApplication.class, args);
		// geraSenha();
	}
	
	private static void geraSenha() {
		System.out.println(new BCryptPasswordEncoder().encode("admin"));
	}
}
