package br.com.mvsistema.remessaapi.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.mvsistema.remessaapi.model.Usuario;
import br.com.mvsistema.remessaapi.repository.PermissaoRepository;
import br.com.mvsistema.remessaapi.repository.UsuarioRepository;

@Service
public class AppUserDetailsService implements UserDetailsService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private PermissaoRepository permissaoRepository;
	
//	@Autowired
//	private GrupoRepository grupoRepository;
	
	@Override
	public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException {
		Optional<Usuario> usuarioOptional = usuarioRepository.findByCpf(cpf);
		Usuario usuario = usuarioOptional.orElseThrow(() -> new UsernameNotFoundException("Usuário e/ou senha incorretos"));
		return new UsuarioSistema(usuario, getPermissoes(usuario));
	}

	private Collection<? extends GrantedAuthority> getPermissoes(Usuario usuario) {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		usuario.getPermissoes().forEach(p -> authorities.add(new SimpleGrantedAuthority(p.getRole().toUpperCase())));
		return authorities;
		//return getPermissoes(grupoRepository.findByUsuariosIn(usuario));
	}
	
//	public Collection<? extends GrantedAuthority> getPermissoes(List<Grupo> grupos) {
//		Collection<GrantedAuthority> auths = new ArrayList<>();
//		
//		for (Grupo grupo: grupos) {
//			List<Permissao> lista = permissaoRepository.findByGruposIn(grupo);
//			
//			for (Permissao permissao: lista) {
//				auths.add(new SimpleGrantedAuthority("ROLE_" + permissao.getDescricao()));
//			}
//		}
//		
//		return auths;
//	}

}
