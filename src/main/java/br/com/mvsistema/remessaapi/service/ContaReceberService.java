package br.com.mvsistema.remessaapi.service;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mvsistema.remessaapi.filter.ContaReceberFilter;
import br.com.mvsistema.remessaapi.model.ContaReceber;
import br.com.mvsistema.remessaapi.repository.ContaReceberRepository;

@Service
public class ContaReceberService {

	private String errorDelete = "this record is related to other tables.";

	@Autowired
	private ContaReceberRepository contaReceberRepository;

	@Transactional
	public ContaReceber save(ContaReceber contaReceber) {
		return contaReceberRepository.save(contaReceber);
	}

	public Page<ContaReceber> filter(ContaReceberFilter contaReceberFilter, Pageable pageable) {
		return contaReceberRepository.filtrar(contaReceberFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			contaReceberRepository.delete(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}

	public ContaReceber buscarContaReceberPeloCodigo(Long codigo) {
		ContaReceber contaReceberSalva = contaReceberRepository.findOne(codigo);
		if (contaReceberSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return contaReceberSalva;
	}

	public ContaReceber atualizar(Long codigo, ContaReceber contaReceber) {
		ContaReceber contaReceberSalva = buscarContaReceberPeloCodigo(codigo);

		BeanUtils.copyProperties(contaReceber, contaReceberSalva, "id");
		return contaReceberRepository.save(contaReceberSalva);
	}
	
	public BigDecimal totalRecebimentos(ContaReceberFilter contaReceberFilter) {
		return contaReceberRepository.totalRecebimentos(contaReceberFilter);
	}

}
