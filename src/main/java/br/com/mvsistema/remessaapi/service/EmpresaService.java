package br.com.mvsistema.remessaapi.service;


import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mvsistema.remessaapi.model.Empresa;
import br.com.mvsistema.remessaapi.repository.EmpresaRepository;
import br.com.mvsistema.remessaapi.filter.EmpresaFilter;

@Service
public class EmpresaService {

	private String errorDelete = "this record is related to other tables.";

    @Autowired
	private EmpresaRepository empresaRepository;

	@Transactional
	public Empresa save(Empresa empresa) {
		return empresaRepository.save(empresa);
	}
	
	public Page<Empresa> filter(EmpresaFilter empresaFilter, Pageable pageable) {
		return empresaRepository.filtrar(empresaFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			empresaRepository.delete(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}
	
	public Empresa buscarEmpresaPeloCodigo(Long codigo) {
		Empresa empresaSalva = empresaRepository.findOne(codigo);
		if (empresaSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return empresaSalva;
	}
	
	public Empresa atualizar(Long codigo, Empresa empresa) {
		Empresa empresaSalva = buscarEmpresaPeloCodigo(codigo);
		
		BeanUtils.copyProperties(empresa, empresaSalva, "id");
		return empresaRepository.save(empresaSalva);
	}

}
