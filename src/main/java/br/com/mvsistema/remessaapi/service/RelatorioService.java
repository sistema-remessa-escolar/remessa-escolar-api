package br.com.mvsistema.remessaapi.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class RelatorioService {

	public static final String SEPARADOR = File.separator;
	
	public static final int PDF = 1;
	public static final int WORD = 2;
	public static final int EXCEL = 3;
	public static final int POWER_POINT = 4;
	public static final int HTML = 5;
	public static final int PLANILHA_OPEN_OFFICE = 6;

	private static Map<String, Object> parametros;
	private static Collection<?> dados;
	private static String nomeArquivoJasper;

	private static void atualizarParametros() {
		dados = (Collection<?>) parametros.get("dados");
		nomeArquivoJasper = (String) parametros.get("nomeArquivoJasper");
		parametros.remove("dados");
		parametros.remove("nomeArquivoJasper");
		System.out.println("Atualizando parametros...");
	}
	
	/**
	 * Metodo que gera o relatorio dados - Lista de dados para ser
	 * impresso(Parametros). nomeArquivoJasper - arquico Jasper que será
	 * impresso(Parametros).
	 * 
	 * @param param
	 * @throws FileNotFoundException 
	 * @throws JRException
	 * @throws IOException
	 */
	public byte[] gerarPdf(Map<String, Object> param) {
		try {
			parametros = param;

			atualizarParametros();
			
			String jasper = "/relatorios/"+nomeArquivoJasper+".jasper";// SEPARADOR.concat("relatorios").concat(SEPARADOR).concat(nomeArquivoJasper).concat(".jasper");
			
			System.out.println("Caminho do jasper: " + jasper);
						
			InputStream isSub = this.getClass().getResourceAsStream(jasper);
			
			param.put("REPORT_LOCALE", new Locale("pt", "BR"));
			param.put("SUB_REPORT_PATH",isSub);
			param.put("SUB_REPORT_DATA_SOURCE",dados);
			
			System.out.println("Caminho do subReport: " + param.get("SUB_REPORT_PATH").toString());
			
			String jasperPadrao = "/relatorios/padrao/padraoPortatil.jrxml"; // SEPARADOR.concat("relatorios").concat(SEPARADOR).concat("padrao").concat(SEPARADOR).concat("padraoPortatil").concat(".jrxml");
			
			InputStream is = this.getClass().getResourceAsStream(jasperPadrao);
			
			System.out.println("Dados do relatorio:\nArquivo: "+ jasperPadrao+" \nInputStream:" + is + "\nParametros: " + param + "\nLista de Dados: " + dados.size());
			
			JasperReport report = JasperCompileManager.compileReport(is);
			
			JasperPrint print = JasperFillManager.fillReport(report, param, new JRBeanCollectionDataSource(dados));
			
			System.out.println("Fazendo impressao do relatorio");
						
			return JasperExportManager.exportReportToPdf(print);

		} catch (JRException e) {
			System.err.println("Erro ao imprimir relatorio:\n" + e.getMessage());
			e.printStackTrace();
		} 

		return null;

	}
}
