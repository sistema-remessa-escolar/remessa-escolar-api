package br.com.mvsistema.remessaapi.service;


import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mvsistema.remessaapi.model.CondicaoPagamento;
import br.com.mvsistema.remessaapi.repository.CondicaoPagamentoRepository;
import br.com.mvsistema.remessaapi.filter.CondicaoPagamentoFilter;

@Service
public class CondicaoPagamentoService {

	private String errorDelete = "this record is related to other tables.";

    @Autowired
	private CondicaoPagamentoRepository condicaoPagamentoRepository;

	@Transactional
	public CondicaoPagamento save(CondicaoPagamento condicaoPagamento) {
		return condicaoPagamentoRepository.save(condicaoPagamento);
	}
	
	public Page<CondicaoPagamento> filter(CondicaoPagamentoFilter condicaoPagamentoFilter, Pageable pageable) {
		return condicaoPagamentoRepository.filtrar(condicaoPagamentoFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			condicaoPagamentoRepository.delete(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}
	
	public CondicaoPagamento buscarCondicaoPagamentoPeloCodigo(Long codigo) {
		CondicaoPagamento condicaoPagamentoSalva = condicaoPagamentoRepository.findOne(codigo);
		if (condicaoPagamentoSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return condicaoPagamentoSalva;
	}
	
	public CondicaoPagamento atualizar(Long codigo, CondicaoPagamento condicaoPagamento) {
		CondicaoPagamento condicaoPagamentoSalva = buscarCondicaoPagamentoPeloCodigo(codigo);
		
		BeanUtils.copyProperties(condicaoPagamento, condicaoPagamentoSalva, "id");
		return condicaoPagamentoRepository.save(condicaoPagamentoSalva);
	}

}
