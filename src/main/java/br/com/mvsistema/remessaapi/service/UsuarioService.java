package br.com.mvsistema.remessaapi.service;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.mvsistema.remessaapi.model.Usuario;
import br.com.mvsistema.remessaapi.repository.UsuarioRepository;
import br.com.mvsistema.remessaapi.filter.UsuarioFilter;

@Service
public class UsuarioService {

	private String errorDelete = "this record is related to other tables.";

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Transactional
	public Usuario save(Usuario usuario) {
		usuario.setSenha(passwordEncoder.encode(usuario.getSenha()));
		return usuarioRepository.save(usuario);
	}

	public Page<Usuario> filter(UsuarioFilter usuarioFilter, Pageable pageable) {
		return usuarioRepository.filtrar(usuarioFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			usuarioRepository.delete(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}

	public Usuario buscarUsuarioPeloCodigo(Long codigo) {
		Usuario usuarioSalva = usuarioRepository.findOne(codigo);
		if (usuarioSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return usuarioSalva;
	}

	public Usuario atualizar(Long codigo, Usuario usuario) {
		Usuario usuarioSalva = buscarUsuarioPeloCodigo(codigo);

		if (!usuario.getSenha().equals("")) {
			usuario.setSenha(passwordEncoder.encode(usuario.getSenha()));
			BeanUtils.copyProperties(usuario, usuarioSalva, "id");
		}
			
		return usuarioRepository.save(usuarioSalva);
	}
}
