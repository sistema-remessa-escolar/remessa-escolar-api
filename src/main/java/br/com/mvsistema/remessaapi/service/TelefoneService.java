package br.com.mvsistema.remessaapi.service;


import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mvsistema.remessaapi.model.Telefone;
import br.com.mvsistema.remessaapi.repository.TelefoneRepository;
import br.com.mvsistema.remessaapi.filter.TelefoneFilter;

@Service
public class TelefoneService {

	private String errorDelete = "this record is related to other tables.";

    @Autowired
	private TelefoneRepository telefoneRepository;

	@Transactional
	public Telefone save(Telefone telefone) {
		return telefoneRepository.save(telefone);
	}
	
	public Page<Telefone> filter(TelefoneFilter telefoneFilter, Pageable pageable) {
		return telefoneRepository.filtrar(telefoneFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			telefoneRepository.delete(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}
	
	public Telefone buscarTelefonePeloCodigo(Long codigo) {
		Telefone telefoneSalva = telefoneRepository.findOne(codigo);
		if (telefoneSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return telefoneSalva;
	}
	
	public Telefone atualizar(Long codigo, Telefone telefone) {
		Telefone telefoneSalva = buscarTelefonePeloCodigo(codigo);
		
		BeanUtils.copyProperties(telefone, telefoneSalva, "id");
		return telefoneRepository.save(telefoneSalva);
	}

}
