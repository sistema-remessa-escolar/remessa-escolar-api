package br.com.mvsistema.remessaapi.service;


import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mvsistema.remessaapi.model.Escola;
import br.com.mvsistema.remessaapi.repository.EscolaRepository;
import br.com.mvsistema.remessaapi.filter.EscolaFilter;

@Service
public class EscolaService {

	private String errorDelete = "this record is related to other tables.";

    @Autowired
	private EscolaRepository escolaRepository;

	@Transactional
	public Escola save(Escola escola) {
		return escolaRepository.save(escola);
	}
	
	public Page<Escola> filter(EscolaFilter escolaFilter, Pageable pageable) {
		return escolaRepository.filtrar(escolaFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			escolaRepository.delete(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}
	
	public Escola buscarEscolaPeloCodigo(Long codigo) {
		Escola escolaSalva = escolaRepository.findOne(codigo);
		if (escolaSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return escolaSalva;
	}
	
	public Escola atualizar(Long codigo, Escola escola) {
		Escola escolaSalva = buscarEscolaPeloCodigo(codigo);
		
		BeanUtils.copyProperties(escola, escolaSalva, "id");
		return escolaRepository.save(escolaSalva);
	}

}
