package br.com.mvsistema.remessaapi.service;


import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mvsistema.remessaapi.model.Produto;
import br.com.mvsistema.remessaapi.repository.ProdutoRepository;
import br.com.mvsistema.remessaapi.filter.ProdutoFilter;

@Service
public class ProdutoService {

	private String errorDelete = "this record is related to other tables.";

    @Autowired
	private ProdutoRepository produtoRepository;

	@Transactional
	public Produto save(Produto produto) {
		return produtoRepository.save(produto);
	}
	
	public Page<Produto> filter(ProdutoFilter produtoFilter, Pageable pageable) {
		return produtoRepository.filtrar(produtoFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			produtoRepository.delete(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}
	
	public Produto buscarProdutoPeloCodigo(Long codigo) {
		Produto produtoSalva = produtoRepository.findOne(codigo);
		if (produtoSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return produtoSalva;
	}
	
	public Produto atualizar(Long codigo, Produto produto) {
		Produto produtoSalva = buscarProdutoPeloCodigo(codigo);
		
		BeanUtils.copyProperties(produto, produtoSalva, "id");
		return produtoRepository.save(produtoSalva);
	}

}
