package br.com.mvsistema.remessaapi.service;


import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mvsistema.remessaapi.model.Permissao;
import br.com.mvsistema.remessaapi.model.dto.PermissaoDTO;
import br.com.mvsistema.remessaapi.repository.PermissaoRepository;
import br.com.mvsistema.remessaapi.filter.PermissaoFilter;

@Service
public class PermissaoService {

	private String errorDelete = "this record is related to other tables.";

    @Autowired
	private PermissaoRepository permissaoRepository;

	@Transactional
	public Permissao save(Permissao permissao) {
		return permissaoRepository.save(permissao);
	}
	
	public Page<Permissao> filter(PermissaoFilter permissaoFilter, Pageable pageable) {
		return permissaoRepository.filtrar(permissaoFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			permissaoRepository.delete(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}
	
	public Permissao buscarPermissaoPeloCodigo(Long codigo) {
		Permissao permissaoSalva = permissaoRepository.findOne(codigo);
		if (permissaoSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return permissaoSalva;
	}
	
	public List<PermissaoDTO> listaPermissaoPaiAndFilho(){
		List<PermissaoDTO> permissoes = new ArrayList<PermissaoDTO>();
		
		List<Permissao> permissaoPai = permissaoRepository.listaPermisssaoPai();
		for (Permissao permissao : permissaoPai) {
			
			PermissaoDTO permissaoDTO = new PermissaoDTO();
			permissaoDTO.setPermissaoPai(permissao);
			
			List<Permissao> permissaoFilhos = permissaoRepository.listaPermisssaoFilho(permissao);
			List<Permissao> permissaoFilhosDTO = new ArrayList<>();
			
			for (Permissao permissao2 : permissaoFilhos) {
				permissaoFilhosDTO.add(permissao2);
			}
			
			permissaoDTO.setListaPermissaoFilhos(permissaoFilhosDTO);
			permissoes.add(permissaoDTO);
		}
		
		return permissoes;
	}
	
	public Permissao atualizar(Long codigo, Permissao permissao) {
		Permissao permissaoSalva = buscarPermissaoPeloCodigo(codigo);
		
		BeanUtils.copyProperties(permissao, permissaoSalva, "id");
		return permissaoRepository.save(permissaoSalva);
	}

}
