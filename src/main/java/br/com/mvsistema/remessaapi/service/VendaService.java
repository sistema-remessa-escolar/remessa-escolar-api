package br.com.mvsistema.remessaapi.service;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mvsistema.remessaapi.filter.VendaFilter;
import br.com.mvsistema.remessaapi.model.Empresa;
import br.com.mvsistema.remessaapi.model.Item;
import br.com.mvsistema.remessaapi.model.Venda;
import br.com.mvsistema.remessaapi.model.dto.RelatorioItensNaoEntregues;
import br.com.mvsistema.remessaapi.repository.ProdutoRepository;
import br.com.mvsistema.remessaapi.repository.VendaRepository;
import net.sf.jasperreports.engine.JRException;

@Service
public class VendaService {

	private String errorDelete = "this record is related to other tables.";

	public static String SEPARADOR = File.separator;

	@Autowired
	private VendaRepository vendaRepository;

	@Autowired
	private ProdutoRepository produtoRepository;

	@Autowired
	private RelatorioService relatorio;

	@Transactional
	public Venda save(Venda venda) {
		venda.setCadastro(LocalDate.now());
		venda.getItens().forEach(item -> {
			item.setVenda(venda);
			item.setProduto(produtoRepository.findOne(item.getProduto().getId()));
			
			if(item.getDataEntrega().equals(null)) {
				item.setDataEntrega(venda.getDataEntrega());
			}
		});

		BigDecimal totalItens = BigDecimal.ZERO;

		for (Item item : venda.getItens()) {
			BigDecimal total = item.getValor().multiply(item.getQuantidade());
			totalItens = totalItens.add(total);
		}
		
		venda.getContasReceber().forEach(cr -> {
			cr.setVenda(venda);
			cr.setDevedor(venda.getEscola());
		});

		venda.setTotal(totalItens);

		return vendaRepository.save(venda);
	}

	public Page<Venda> filter(VendaFilter vendaFilter, Pageable pageable) {
		return vendaRepository.filtrar(vendaFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			vendaRepository.delete(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}

	public Venda buscarVendaPeloCodigo(Long codigo) {
		Venda vendaSalva = vendaRepository.findOne(codigo);
		if (vendaSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return vendaSalva;
	}

	public Venda atualizar(Long codigo, Venda venda) {
		Venda vendaSalva = buscarVendaPeloCodigo(codigo);
		/**
		 * Corrigindo o erro: HibernateException - A collection with
		 * cascade="all-delete-orphan" was no longer referenced by the owning entity
		 * instance Colocando essa iteração na lista de Itens e usando o saveAndFlush
		 */
		venda.getItens().forEach(item -> {
			item.setVenda(vendaSalva);
		});
		
		venda.getContasReceber().forEach(cr -> {
			cr.setVenda(vendaSalva);
			cr.setDevedor(vendaSalva.getEscola());
		});
				
		BeanUtils.copyProperties(venda, vendaSalva, "id");
		return vendaRepository.saveAndFlush(vendaSalva);
	}

	public byte[] relatorioPedidoRemessa(Long id) throws JRException {
		Venda relatorioPedido = this.buscarVendaPeloCodigo(id);
		Map<String, Object> parametros = new HashMap<>();

		parametros.put("EMPRESA", relatorioPedido.getEmpresa());
		parametros.put("ENDERECO_EMPRESA", relatorioPedido.getEmpresa().getEndereco());
		parametros.put("CLIENTE", relatorioPedido.getEscola());
		parametros.put("ENDERECO_CLIENTE", relatorioPedido.getEscola().getEndereco());
		parametros.put("VENDA", relatorioPedido);
		parametros.put("dados", relatorioPedido.getItens());
		parametros.put("nomeArquivoJasper", "PedidoRemessa");

		return relatorio.gerarPdf(parametros);
	}

	public byte[] relatorioItensNaoentregues(LocalDate de, LocalDate ate) throws Exception {
		List<RelatorioItensNaoEntregues> ine = new ArrayList<RelatorioItensNaoEntregues>();

		for (Item i : vendaRepository.itensNaoEntreguesNaData(de, ate)) {
			RelatorioItensNaoEntregues relatorio = new RelatorioItensNaoEntregues();
			relatorio.setCodigoProduto(i.getProduto().getId());
			relatorio.setDataEntrega(i.getDataEntrega());
			relatorio.setEmpresa(i.getVenda().getEmpresa());
			relatorio.setNomeCliente(i.getVenda().getEscola().getRazaoSocial());
			relatorio.setNomeProduto(i.getProduto().getNomeProduto());
			relatorio.setQuantidadeFalta(i.getQtdeFalta());
			ine.add(relatorio);
		}

		if (ine != null && ine.size() > 0) {
			Empresa empresa = ine.get(0).getEmpresa();
			Map<String, Object> parametros = new HashMap<>();

			parametros.put("EMPRESA", empresa);
			parametros.put("ENDERECO_EMPRESA", empresa.getEndereco());
			parametros.put("DATA_DE", java.sql.Date.valueOf(de));
			parametros.put("DATA_ATE", java.sql.Date.valueOf(ate));
			parametros.put("dados", ine);
			parametros.put("nomeArquivoJasper", "ItensNaoEntregues");

			return relatorio.gerarPdf(parametros);
		}
		throw new Exception("Sem dados no filtro informado"); //EmptyResultDataAccessException(, 1);
	}
	
	public BigDecimal totalVendas(VendaFilter vendaFilter) {
		return this.vendaRepository.totalVenas(vendaFilter);
	}
}
