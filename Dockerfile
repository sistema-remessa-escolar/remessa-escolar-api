FROM openjdk:8-jdk-alpine
ADD target/remessa-api-1.0.0.Beta6.jar app.jar
ENTRYPOINT [ "sh", "-c", "java -jar /app.jar" ]